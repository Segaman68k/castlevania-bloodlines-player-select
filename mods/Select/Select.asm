;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; ROM SPACE
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

 if endofrom = 0

; ---------------------------------------------------------------------------
; set our controls suproutine
; ---------------------------------------------------------------------------

    ; ingame
	org $D176
	jmp subControls

; ---------------------------------------------------------------------------


 endif
 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; END OF ROM SPACE
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

 if endofrom = 1

; ---------------------------------------------------------------------------
; VARIABLES
; ---------------------------------------------------------------------------

; sub menu variable
PAD1            equ $FF9060
PAD1PR          equ $FF9061
PLAYERTYPE      equ $FFFB08
BEHAVIORTYPE    equ $FF9CE8

; ---------------------------------------------------------------------------
; Execute while pause is active
; ---------------------------------------------------------------------------

subControls:
    ; check B pressed
    btst    #BSTART, (PAD1PR).l
    beq     locSkipCheck

    ; check A hold
    btst    #BB, (PAD1).l
    beq     locSkipCheck

    ; transform
    tst.w   PLAYERTYPE
    beq     subChangeToEric
    bra     subChangeToJohn

locSkipCheck:
    ; execute old code
    btst    #BSTART,PAD1PR
    beq     lscLocret
    jmp     $D180

lscLocret:
    rts

; ---------------------------------------------------------------------------
; Change players
; ---------------------------------------------------------------------------

subChangeToEric:
    move.w  #1,PLAYERTYPE
    move.w  #$40,BEHAVIORTYPE
    rts ;bra     locSkipCheck

subChangeToJohn:
    move.w  #0,PLAYERTYPE
    move.w  #0,BEHAVIORTYPE
    rts ;bra     locSkipCheck

; ---------------------------------------------------------------------------

 endif
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
