@echo off

cls

set AS_MSGPATH=bin/msg
set USEANSI=n

bin\asw  -xx -c -A Castlevania.asm
IF NOT EXIST Castlevania.p goto LABLPAUSE
IF EXIST Castlevania.p goto Convert2ROM
:Convert2ROM
bin\p2bin Castlevania.p out\CastlevaniaPlayerSelect.bin -l 0 -r $-$
bin\rompad.exe out\CastlevaniaPlayerSelect.bin 255 0
bin\fixheadr.exe out\CastlevaniaPlayerSelect.bin
bin\flips.exe -c -i data\cvb.bin out\CastlevaniaPlayerSelect.bin
del Castlevania.p
del Castlevania.h
exit /b
:LABLPAUSE

pause


exit /b
