sjsr macro adress
        dc.w $4EBA,adress-*-2
        endm
slea0 macro adress,align
        dc.w $41FA,adress-*-2
        endm
slea1 macro adress,align
        dc.w $43FA,adress-*-2
        endm
slea2 macro adress,align
        dc.w $45FA,adress-*-2
        endm
slea3 macro adress,align
        dc.w $47FA,adress-*-2
        endm
slea4 macro adress,align
        dc.w $49FA,adress-*-2
        endm
slea5 macro adress,align
        dc.w $4BFA,adress-*-2
        endm
slea6 macro adress,align
        dc.w $4DFA,adress-*-2
        endm
slea7 macro adress,align
        dc.w $4FFA,adress-*-2
        endm

; ---------------------------------------------------------------------------
; Buttons Bits
; ---------------------------------------------------------------------------

BUP     equ 0
BDOWN   equ 1
BLEFT   equ 2
BRIGHT  equ 3
BB      equ 4
BC      equ 5
BA      equ 6
BSTART  equ 7

; ---------------------------------------------------------------------------
; Buttons Bytes
; ---------------------------------------------------------------------------

BTNUP      equ $01
BTNDOWN    equ $02
BTNLEFT    equ $04
BTNRIGHT   equ $08
BTNB       equ $10
BTNC       equ $20
BTNA       equ $40
BTNSTART   equ $80
