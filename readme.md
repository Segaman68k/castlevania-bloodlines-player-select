**Castlevania Bloodlines Player Select**
(C) **Segaman68k**

It's a hack for **Sega Genesis/Mega Drive** game **"Castlevania Bloodlines"**

It alllows to change player on a fly.

Just press **B** + **START** while playing.

# Building

How to build hack using sources:

1. Create **"out"** and **"data"** directories

2. Place *Castlevania - Bloodlines (U) [!]* rom into **"data"** directory

3. Rename it **"cvb.bin"**. Result: *data/cvb.bin*

4. Run **"compile.bat"**

The result *ROM* and *IPS* will be placed into **"out"** directory

# Hints

This sources using double pass compiling.

All modification stored inside **"mods"** directory.

All mods will be pass twice during compilation.

Every mod compiled twice with the flag **"endofrom"** changed.

It tells that building is running *Inside* or *Outside* of ROM.

That means, if you want to place something *Inside* ROM using **"org"**, check for **"endofrom"** is equal to zero

**Example:**

------------------

```
 if endofrom = 0

 org $1000
 
 jmp myRoutine
 
 endif
```

------------------

Everything that placed *Outside* ROM is `endofrom = 1`.

That means everything you put *Outisde* ROM will be stored in *Free Space*, which gives you 2 rules:

1. Set **"org"** addres in main **"asm"** file inside section **"END OF ROM SPACE"** to place, where ROM memory is free to use (like array of *$FF*'s at the end of ROM). Be sure that space is enough for your routines.

2. Never use **"org"** when your routines placed inside `endofrom = 1`

# Download

Look for *download* section of **BitBucket** to get *ips* patches.

Use **"bin/flips.exe"** to apply patch.

Apply patch only to *Castlevania - Bloodlines (U) [!]* version of ROM

# Links

https://segaman.top/

